from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.core.window import Window

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        build = Builder.load_file("bottom_navigation_to_label.kv")
        return build

if __name__=="__main__":
    App().run()