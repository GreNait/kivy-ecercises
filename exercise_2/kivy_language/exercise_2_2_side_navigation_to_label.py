from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.core.window import Window

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        build = Builder.load_file("side_navigation_to_label.kv")
        return build

if __name__=="__main__":
    App().run()