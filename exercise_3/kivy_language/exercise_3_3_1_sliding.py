from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.core.window import Window
from kivy.uix.pagelayout import PageLayout

class PageLayoutExample(PageLayout):
    Builder.load_file("sliding.kv")

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        return PageLayoutExample()

if __name__=="__main__":
    App().run()