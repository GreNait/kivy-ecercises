from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.core.window import Window
from kivymd.uix.boxlayout import MDBoxLayout

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        build = Builder.load_file("box_layout.kv")
        return build

if __name__=="__main__":
    App().run()