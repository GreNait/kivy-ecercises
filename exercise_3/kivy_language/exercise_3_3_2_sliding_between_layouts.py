from kivy.core.window import Window
from kivy.lang import Builder
from kivy.uix.pagelayout import PageLayout
from kivymd.app import MDApp
from kivymd.uix.screen import MDScreen
from kivymd.uix.button import MDRoundFlatButton

Builder.load_file("button_grid.kv")
Builder.load_file("box_layout_page_layout.kv")
Builder.load_file("page_layout.kv")

class ButtonGrid(MDScreen):
    pass

class BoxLayoutExample(MDScreen):
    def add_button(self):
        button = MDRoundFlatButton(
            text="button",
            pos_hint={"center_x":.5}
        )

        button.on_release = self.add_button
        self.ids.box.add_widget(button)

class PageLayoutExample(PageLayout):
    pass
       
class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        page_layout = PageLayoutExample()
        return page_layout

if __name__=="__main__":
    App().run()
