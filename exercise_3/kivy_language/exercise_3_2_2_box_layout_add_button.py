from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.core.window import Window
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.button import MDFillRoundFlatButton

class BoxLayoutExample(MDBoxLayout):
    build = Builder.load_file("box_layout_add_button.kv")

    def add_button(self):
        button = MDFillRoundFlatButton(
                text="button",
                pos_hint={"center_x":.5},
            )

        button.on_release = self.add_button

        self.add_widget(button)

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        return BoxLayoutExample()

if __name__=="__main__":
    App().run()
