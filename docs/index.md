# Kivy & KivyMD exercises

It is rather hard to start a sophisticated project with tools, which you don't know yet. It might be quite frustrating. I thought, that it might be a good start to have some easy/simple exercises for Kivy/KivyMD. With these exercises, you will be able to learn basics of Kivy and the `KV language. Combining several exercises might already be enough for first app layouts.

You can start with the first exercises here: [Exercise 1](exercises/exercise_1.md)
