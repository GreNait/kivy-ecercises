# Using layouts

Layouts are a simple tool for arranging widgets. However, the implementation for the layouts differs in complexits. E.g. using a gridlayout does not mean that a widget put into a grid tile is centered. Actually, the grid tile size is "adjusted" to the widget. This might be contra productive.

## Gridlayout

Using a grid layout should make it simpler to arrange widgets within a grid. If you want to center, e.g. a button within a grid tile, you need to either use complicated nested anchor layouts, or screens. E.g.

```kivy
#:kivy 2.1.0

MDGridLayout:
    cols: 2
    
    MDScreen:
        MDRectangleFlatButton:
            pos_hint: {"center_x":.5, "center_y":.5}
            text: "button1"
    MDScreen:
        MDRectangleFlatButton:
            pos_hint: {"center_x":.5, "center_y":.5}
            text: "button2"

    MDScreen:
        MDRectangleFlatButton:
            pos_hint: {"center_x":.5, "center_y":.5}
            text: "button3"

    MDScreen:
        MDIconButton:
            pos_hint: {"center_x":.5, "center_y":.5}
            icon: "apple"
```

![ButtonsInGrid](../gifs/exercise_3_1.gif)

Without the screens, you will need to change the widget size. However, MDButtons cannot be changed in size (at least not easily) and they adapt automatically to the text etc.

## Boxlayout

A box layout can be orientated `vertical` or `horizontal`.

_*Note: Layouts can also be positioned with a pos hint.*_

Simple example:

```kivy
#:kivy 2.1.0

MDBoxLayout:
    orientation: "vertical"
    pos_hint: {"center_y":0.5}
    adaptive_height: True
    spacing: 20

    MDFillRoundFlatButton:
        text: "button 1"
        pos_hint: {"center_x":.5}


    MDFillRoundFlatButton:
        text: "button 2"
        pos_hint: {"center_x":.5}


    MDFillRoundFlatButton:
        text: "button 3"
        pos_hint: {"center_x":.5}
        

    MDFillRoundFlatButton:
        text: "button 4"
        pos_hint: {"center_x":.5}
```

Which lead to:

![BoxLayout](../gifs/exercise_3_2_1.gif)

## Pagelayout

_*Note: If the widgets don't have different colors, they are most likly transparent. Meaning, you can see the other widget "shining" through*_

Easy way to implement a page layout (and using the sliding effect) is via the `.kv` file.

```kivy
#:kivy 2.1.0

<PageLayoutExample>:
    MDScreen:
        md_bg_color: 1, 0, 0, 1
        MDLabel:
            halign: "center"
            text: "Screen 1"

    MDScreen:
        md_bg_color: 0, 1, 0, 1
        MDLabel:
            halign: "center"
            text: "Screen 2"

    MDScreen:
        md_bg_color: 0, 0, 1, 1
        MDLabel:
            halign: "center"
            text: "Screen 3"
```

The python part is also fairly simple:

```python
...
class PageLayoutExample(PageLayout):
    Builder.load_file("sliding.kv")

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        return PageLayoutExample()

if __name__=="__main__":
    App().run()
```

This leads to:

![Sliding](../gifs/exercise_3_3_1.gif)
