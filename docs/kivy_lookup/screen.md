# Examples of screen/window properties

## Changing the size of the screen/window in python

```python
from kivy.core.window import Window

Window.size = (350,600)
```

## Swichting screens

### In python

To easily switch between screens, it appears that `ScreenManager`s are the best way doing that. Adding several screens to this manager, enables you to switch between those screens, based on their names. You need to use `ScreenManager.current`=`Screen_name` to switch.

*Note: `ScreenManager.next()` provides you the screen name of the next screen in line (also see `previously`).*

```python
from kivy.uix.screenmanager import ScreenManager
...
class Screen1:
    ...
    def switch_to(self):
        self.screen_manager.current = "Screen2"
    ...

class Screen2:
    ...
    def switch_to(self):
        self.screen_manager.current = "Screen1"
    ...
...
class MainScreenApp(MDApp):
    def build(self):
        sm = ScreenManager()
        sm.add_widget(Screen1(name="Screen1", screen_manager=sm))
        sm.add_widget(Screen2(name="Screen2", screen_manager=sm))

        return sm
...
if __name__=="__main__":
    MainScreenApp().run()
```

### In kivy

It feels simpler to use the kivy language instead of pure python for navigating between screens. It needs some adjustments to get into the `.kv` idea, but it appears to be worth it.

The `ScreenManager` is still the best approach for switching between screens. A combination of simple python code and the kv file enables the switching.

Here are some helpers to read both, python and kivy:

- _`<Screen>`_ - is a placeholder. Needs to be implemented as a python class (can be empty). This python class will implement the complete widget structure etc. from `<Screen>`. It can be any other widget like Layouts, Buttons, etc.
- _`name: SomeName`_ - The simple name of the widget. These names make e.g. the screen identifiable for the screen manager. It also allows to react on different screens from other screens.
- _`root.manager.get_screen("SomeName")`_ - Get access to a screen. This works great if the screen is defined in `.kv` and has a name provided. Otherwise, you need to know the screen name.
- _`id: SomeID`_ - Is like a variable name. However, it is only available within the Widget scope. Not accesible from other screens etc.
- _`root.manager.current`_ - Enables you to switch via the screen manager (same as in [above].(#in-python))
- _`on_text: root.manager.get_screen("SecondScreen").ids.input_label.text = self.text`_ - This is a good example on how to change the label within a different screen. `self.text` means, that this definition has been within a Label or other widget with a text property.

First, the python part:

```python
... #inputs and stuff

class FirstScreen(Screen):
    pass

class SecondScreen(Screen):
    pass

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        Builder.load_file("kivy_file.kv")
        sm = ScreenManager()
        sm.add_widget(FirstScreen())
        sm.add_widget(SecondScreen())
        return sm
```

And here the kivy part:

```kivy
#:kivy 2.1.0

<FirstScreen>
    name: "FirstScreen"
    MDScreen:
        MDTextField:
            hint_text: "Type here"
            pos_hint: {"center_x":.5, "center_y":.5}
            on_text: root.manager.get_screen("SecondScreen").ids.input_label.text = self.text

            
        MDRectangleFlatIconButton:
            icon: "language-python"
            text: "Next"
            pos_hint: {"center_x":.5, "center_y":.4}
            on_release: root.manager.current="SecondScreen"

<SecondScreen>
    name: "SecondScreen"
    MDScreen:
        MDLabel:
            id: input_label
            text: "Default"
            halign: "center"

        MDRectangleFlatIconButton:
            icon: "android"
            text: "Back"
            pos_hint: {"center_x":.5, "center_y":.4}
            on_release: root.manager.current="FirstScreen"
```

## Provide data to next screen via python

I haven't figured out any better way on poviding data to the next screen. However, it is possible to change the status of the next screen. This is fairly complicated in python and I guess here lies the strength of the KV language.

```python
self._screen_manager.get_screen(self._screen_manager.next()).children[0].text = "Text for e.g. label"
```

This could be done within a callback function, before switching the screen:

```python
def next_screen(self)->None:
    self._screen_manager.get_screen(self._screen_manager.next()).children[0].text = self.text_input.text
    self._screen_manager.current = self._screen_manager.next()
```

In this example `children[0]` is a label. I am sure, there is a better way to handle this, I just haven't found it yet.

## Other approach to share data with the next screen

Accessing directly the next screen via the screen manager is a highly coupled system. You need to know the label etc. and it is not easy changing either class. Another approach, would be the publisher/subscriber (callbacks) approach. A simple dataclass might already solve the problem of updating the label. Additional advantage: several other parts (perhaps even screens) of the program can subscribe to this value.

A simple dataclass for changing the text label and updating the subscriber could look like this:

```python
@dataclass
class SharedInformation:
    label_text : str
    subscriber : list = field(default_factory=list)

    def add_subscriber(self, subs):
        self.subscriber.append(subs)

    def notify_subscriber(self, val):
        if hasattr(self, "subscriber"):
            for subs in self.subscriber:
                subs.update_label_text(val)

    def __setattr__(self, prop, val):
        super().__setattr__(prop, val)
        if prop == "label_text":
            self.notify_subscriber(val)
```

This can be easily used now within the screens after registering this screen with the dataclass:

```python
class SomeScreen:
    ...
    def update_label_text(self, text):
        self.label.text = text

...
some_screen = SomeScreen(...)
shared_information = SharedInformation("Default")
shared_information.add_subscriber(some_screen)
...

```

## Adding a widget

You might want to add a widget like:

```python
self.add_widget(SomeWidget())
```

It is also possible to add a widget to a child. Best use `id`s for that.

```kivy
MDBoxLayout:
    id: box
    ...
```

```python
self.ids.box.add_widget(SomeWidget())
```