# Examples of button properties

## Creating a button in Python

```python
from kivymd.uix.button import MDRectangleFlatButton

button = MDRectangleFlatButton(
        text="Button 1",
        pos_hint={"center_x":0.5, "center_y":0.5},
    )
```

### Reacting on e.g. `on_release`

```python
def callback(self):
    print("pushed")

button.on_release = self.callback
```

*Note: I haven't figured out yet, on how to provide information within the callback*
