# Using the kivy language `.kv`

_*Note: See documentation: [Kivy language](https://kivy.org/doc/stable/guide/lang.html#)*_

## Referencing class Widgets

A class widget looks within kivy like:

```kivy
<BoxLayoutExample>:
```

_*Note: Don't forget the colon `:`*_

A class widget needs a reference within the python files.

```python
...
class BoxLayoutExample(MDBoxLayout):
    #stuff
...
```

The nice thing is, that all methods within this python class, are also available for all widgets defined wihin the `.kv` file via the root keyword.

```python
...
class BoxLayoutExample(MDBoxLayout):
    def test(self):
        print("Test")
...
```

```kivy
<BoxLayoutExample>:
    MDFillRoundFlatButton:
        text: "button 1"
        pos_hint: {"center_x":.5}
        on_release: root.test()
```

## Loading several `.kv` files and referencing each other

Through the `Builder` it is easy to load several `.kv` files. This could be done directly after the imports. You need to create a class for all referenced class widgets.

```python
#imports
from kivy.lang import Builder
...

Builder.load_file("button_grid.kv")
Builder.load_file("box_layout_page_layout.kv")
Builder.load_file("page_layout.kv")
```

If one of these kivy files contains a `<ClassExample>:`, you need to add the class.

```python
class ClassExample(MDScreen):
    pass
```

It is also possible to reference this, or several other class widgets within a different `.kv` file. This could look like:

```kivy
#:kivy 2.1.0

<PageLayoutExample>:
    MDScreen:
        md_bg_color: 1,0,0,1
        MDLabel:
            halign: "center"
            text: "Screen 1"

    ButtonGrid:

    BoxLayoutExample:
```