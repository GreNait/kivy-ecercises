# Screen navigation 

Switching between screens can be implemented manually via button inputs and a screen manager. It is also possible to use different navigation tools (`bottom navigation`|| `sidebar navigation`). These simplify the screen switching. The navigation is already functioning as a screen manager.


## Bottom navigation

The easiest approach is defining the whole navigation in the kivy language:

```kivy
#:kivy 2.1.0

MDBottomNavigation:
    MDBottomNavigationItem:
        name: "FirstScreen"
        icon: "language-python"
        text: "FirstScreen"
        
        MDLabel:
            text: "The first screen"
            halign: "center"

    MDBottomNavigationItem:
        name: "SecondScreen"
        icon: "android"
        text: "SecondScreen"

        MDLabel:
            text: "The second screen"
            halign: "center"

    MDBottomNavigationItem:
        name: "ThirdScreen"
        icon: "apple"
        text: "Screen"

        MDLabel:
            text: "The third screen"
            halign: "center"
```

Within python, you just need to load the kivy file and the switching between the screens works out of the box.

```python
from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.core.window import Window

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        build = Builder.load_file("bottom_navigation_to_label.kv")
        return build

if __name__=="__main__":
    App().run()
```

## Navigation drawer

A navigation drawer opens from the side and can contain buttons etc. Using the drawer is a bit more compicated than the other navigation parts. Combining the drawer and the navigation rail might be a good choice. The rail could have small, simple icons - the drawer would container more information.

It is easiest to achieve the drawer with the kivy language.

```kivy
#:kivy 2.1.0

MDScreen:
    MDToolbar:
        pos_hint: {"top":1}
        title: "Menu"
        left_action_items: [["menu", lambda x: nav_drawer.set_state("open")]]
   
    MDNavigationLayout:
        ScreenManager:
            id: screen_manager
            MDScreen:
                name: "screen_python"
                MDLabel:
                    text: "Python"
                    halign: "center"

            MDScreen:
                name: "screen_cpp"
                MDLabel:
                    text: "C++"
                    halign: "center"

        MDNavigationDrawer:
            id: nav_drawer

            MDGridLayout:
                cols: 1
                
                MDIconButton:
                    icon: "language-python"
                    on_press: screen_manager.current = "screen_python"
                    on_release: nav_drawer.set_state("close")

                MDIconButton:
                    icon: "language-cpp"
                    on_release: screen_manager.current = "screen_cpp"
                    on_release: nav_drawer.set_state("close")
```

The important part is, that (in the case above) the MDIconButtons need to access the screen manager and also close the open drawer afterwards. If this is not set, you need to click outside of the drawer to close it. Opening the drawer needs to haben like closing it. Using the menue button with the toolbar and the `lambda` function is a easy way, but not the only one.