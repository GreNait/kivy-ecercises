# Exercise 2 - Navigation and screen change

## Exercise 2.1 Bottom Navigation to Label

### Description

Use a bottom navigation and change screens through the navigation. Every new screen should have a different label for identification. The label should be placed in the center of the screen.

### Possible preview

![BottomNavigationToLabel](../app_template/bottom_navigation_to_screen.drawio.svg)

### Possible solution

![BottomNavigationToLabel](../gifs/exercise_2_1.gif)

## Exercise 2.2 Side Navigation to Label

### Description

A side navigation bar should open when the navigation button (burger button?) is pushed. Then, navigation between three screens. See [Bottom Navigation](#exercise-21-bottom-navigation-to-label) and use that as a reference.

### Possible preview

![SideNavigationToLabel](../app_template/side_navigation_to_label.drawio.svg)

### Possible solution

![SideNavigationToLabel](../gifs/exercise_2_2.gif)

## Exercise 2.3 Open drawer navigation

### Description

A Navigation drawer should be opened via a menu button (burger button). It should be possible to switch between screens within this drawer navigation.

### Possible preview

![OpenDrawer](../app_template/open_drawer.drawio.svg)

### Possible solution

![OpenDrawer](../gifs/exercise_2_3.gif)
