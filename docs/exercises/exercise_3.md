# Excercise 3 - Layouts

## Exercise 3.1 - 4 Buttons in a grid (GridLayout)

*See documentation [GridLayout](https://kivy.org/doc/stable/api-kivy.uix.gridlayout.html#module-kivy.uix.gridlayout)*

### Description

Position 4 buttons within a 2 by 2 grid. The buttons should not fill the grid tile and should be centered within a grid tile.

### Possible preview

![ButtonsInGrid](../app_template/buttons_ing_grid.drawio.svg)

### Possible solution

![ButtonsInGrid](../gifs/exercise_3_1.gif)

## Exercise 3.2.1 - Buttons are ordered from top to bottom (BoxLayout)
*See documentation [BoxLayout](https://kivy.org/doc/stable/api-kivy.uix.boxlayout.html#module-kivy.uix.boxlayout)*

### Description

Have 4 buttons in a column. They should be evenly spaced between each other.

### Possible preview

![ButtonsInColumn](../app_template/buttons_in_column.drawio.svg)

### Possible solution

![BoxLayout](../gifs/exercise_3_2_1.gif)

## Exercise 3.2.2 - Add button to column

### Description

Use the layout from [Exercise 3.2.1](#exercise-321---buttons-are-ordered-from-top-to-bottom-boxlayout). If a button is pushed, generate a new button and add it to the column.

### Possible preview

![ButtonInColumnAddButton](../app_template/buttons_in_column_add_button.drawio.svg)

### Possible solution

![BoxLayoutAddButton](../gifs/exercise_3_2_2.gif)

## Exercise 3.3.1 - Sliding between screens (PageLayout)

*See documenatation [PageLayout](https://kivy.org/doc/stable/api-kivy.uix.pagelayout.html#module-kivy.uix.pagelayout)*

### Description

Use the slide mechanism (swiping left and right) to switch between screens. The different screens should be identified by a simple label.

### Possible preview

![SlideLeftRight](../app_template/swiping_left_right.drawio.svg)

### Possible solution

![PageLayout](../gifs/exercise_3_3_1.gif)

## Exercise 3.3.2 - Slide between former layouts

### Description

Sliede between the layouts of [Exercise 3.1](#exercise-31---4-buttons-in-a-grid-gridlayout), [Exercise 3.2.2](#exercise-322---add-button-to-column) and a blank screen. Starting with a blank screen.

### Possible preview

![SlideFormerLayouts](../app_template/slide_former_layouts.drawio.svg)

### Possible solution

![SlideThroughLayouts](../gifs/exercise_3_3_2.gif)
