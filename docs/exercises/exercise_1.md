# Exercise 1 - From buttons, inputs and screen change

## Exercise 1.1 - Button to Label

### Description

Build an Kivy/KivyMD app, which shows a button, centered in the middle. When the button is pushed, change the screen and show an label with `pushed`.

### Possible preview

![ButtonToLabel](../app_template/button_to_label.drawio.svg)

### Possible solution

![Gif](../gifs/exercise_1_1.gif)

## Exercise 1.2 - Button to Label to Button

### Description

Use the solution from [exercise 1.1](#exercise-11) and add a button to the second screen. This button is located beneath the label (all centered) and returns to the first screen.

### Possible preview

![ButtonToLabelToButton](../app_template/button_to_label_to_button.drawio.svg)

### Possible solution

![Gif](../gifs/exercise_1_2.gif)

## Exercise 1.3 - Input to Label to Input

### Description

Display an input field (text) and display this content within a label if a button is pushed. The button is located bellow the input. The next screen has a return function. See [exercise 1.2](#exercise-12) for that.

### Possible preview

![InputToLabel](../app_template/inputs_to_label.drawio.svg)

### Possible solution
![Gif](../gifs/exercise_1_3.gif)
