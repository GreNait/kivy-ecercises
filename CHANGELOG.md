# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.6.0 (2022-04-28)

### Feat

- **exercise_3_3_2_sliding_between_layouts.py**: solved exercise 3.3.2
- **exercise_3_3_1_sliding.py**: solved exercise 3.3.1

## 0.5.0 (2022-04-27)

### Feat

- **exercise_3_2_2_boy_layout_add_button.py**: solved exercise 3.2.2
- **exercise_3_2_1_box_layout.py**: Solved exercise 3.2.1
- **exercise_3_1_grid_layout.py**: solved exercise 3.1

## 0.4.0 (2022-04-25)

### Feat

- **exercise_2_3_open_drawer.py**: solved exercise 2.3
- **exercise_2_2_side_navigation_to_label.py**: solved exercise 2.3
- **exercise_2/kivy_language/exercise_2_1_bottom_navigation_to_label.py**: solved exercise 2.1 bottom navigation

## 0.3.0 (2022-04-23)

### Feat

- **kivy_language/exercise_1_3_input_to_label_to_input.py**: solved the exercise 1.3 with the kv language and added documentation about the approach
- **kivy_language/exercise_1_2_button_to_label_to_button.py**: solved exercise 1.2 with the kv language

## 0.2.0 (2022-04-23)

### Feat

- **kivy_language/exercise_1_1_button_to_label.py**: instead of python used the kv language for screen layout etc
- **exercise_1_3_input_to_label_to_input.py**: used a dataclass and the publisher/subscriber pattern for changing the label text
- **exercise_1/exercise_1_3_input_to_label_to_input.py**: solution for exercise 1.3

## 0.1.0 (2022-04-19)

### Feat

- **exercise_1_2_button_to_label_to_button.py,-docs/***: solved exercise 1.2 and added info to documentation (with gifs)
- **exercise_1_1_buuton_to_label.py**: first solution to the exercise and documentation update

### Fix

- **exercise_1/***: first part of the exercise_1
