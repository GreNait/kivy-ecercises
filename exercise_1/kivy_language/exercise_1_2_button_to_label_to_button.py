from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.core.window import Window

class FirstScreen(Screen):
    pass

class SecondScreen(Screen):
    pass

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        Builder.load_file("button_to_label_to_button.kv")
        sm = ScreenManager()
        sm.add_widget(FirstScreen(name="FirstScreen"))
        sm.add_widget(SecondScreen(name="SecondScreen"))
        return sm

if __name__=="__main__":
    App().run()