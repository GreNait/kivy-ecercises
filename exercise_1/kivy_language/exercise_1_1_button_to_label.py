from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.core.window import Window

kv = """
<FirstScreen>
    MDScreen:
        MDRectangleFlatIconButton:
            icon: "python"
            text: "Next"
            pos_hint: {"center_x":.5, "center_y":.5}
            on_release: root.manager.current = "Second Screen"
<SecondScreen>
    MDScreen:
        MDLabel:
            text: "Label"
            halign: "center"
"""

class FirstScreen(Screen):
    pass

class SecondScreen(Screen):
    pass

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        Builder.load_string(kv)
        sm = ScreenManager()
        sm.add_widget(FirstScreen(name="First Screen"))
        sm.add_widget(SecondScreen(name="Second Screen"))
        return sm

if __name__=="__main__":
    App().run()
