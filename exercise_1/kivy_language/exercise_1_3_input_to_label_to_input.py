from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.core.window import Window

class FirstScreen(Screen):
    pass

class SecondScreen(Screen):
    pass

class App(MDApp):
    def build(self):
        Window.size = (350, 600)
        Builder.load_file("input_to_label_to_input.kv")
        sm = ScreenManager()
        sm.add_widget(FirstScreen())
        sm.add_widget(SecondScreen())
        return sm

if __name__=="__main__":
    App().run()