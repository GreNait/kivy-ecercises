from kivymd.uix.screen import Screen
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.app import MDApp

from kivy.uix.screenmanager import ScreenManager

from kivy.core.window import Window


class FirstScreen:
    def __init__(self, name: str, screen_manager: ScreenManager) -> None:
        self._screen_manager = screen_manager
        self._screen = Screen(name=name)
        self.add_button()

    @property
    def screen(self) -> Screen:
        return self._screen

    def next_screen(self) -> None:
        self._screen_manager.transition.direction = "left"
        self._screen_manager.current = self._screen_manager.next()

    def add_button(self) -> None:
        button = MDRectangleFlatButton(
            text="Next", pos_hint={"center_x": 0.5, "center_y": 0.5}
        )

        button.on_release = self.next_screen

        self._screen.add_widget(button)


class SecondScreen:
    def __init__(self, name: str, screen_manager: ScreenManager) -> None:
        self._screen_manager = screen_manager
        self._screen = Screen(name=name)
        self.add_label()
        self.add_button()

    @property
    def screen(self) -> Screen:
        return self._screen

    def previous_screen(self) -> None:
        self._screen_manager.transition.direction = "right"
        self._screen_manager.current = self._screen_manager.previous()

    def add_label(self) -> None:
        label = MDLabel(text="pushed", halign="center")

        self._screen.add_widget(label)

    def add_button(self) -> None:
        button = MDRectangleFlatButton(
            text="Back", pos_hint={"center_x": 0.5, "center_y": 0.4}
        )

        button.on_release = self.previous_screen

        self._screen.add_widget(button)


class App(MDApp):
    def build(self) -> ScreenManager:
        Window.size = (350, 600)

        sm = ScreenManager()
        sm.add_widget(
            FirstScreen(
                name="FirstScreen",
                screen_manager=sm,
            ).screen
        )

        sm.add_widget(
            SecondScreen(
                name="SecondScreen",
                screen_manager=sm,
            ).screen
        )

        return sm


if __name__ == "__main__":
    App().run()
