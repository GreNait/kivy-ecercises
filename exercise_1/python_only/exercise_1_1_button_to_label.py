from kivy.core.window import Window

from kivymd.app import MDApp
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.label import MDLabel
from kivymd.uix.screen import MDScreen
from kivy.uix.screenmanager import ScreenManager


class SecondScreen:
    def build(self):
        screen = MDScreen(name="Screen2")

        screen.add_widget(MDLabel(text="Screen2", halign="center"))

        return screen


class SingleButton:
    def __init__(self, screen_manager):
        self.screen_manager = screen_manager

    def switch_screen(self):
        self.screen_manager.current = self.screen_manager.next()

    def build(self):
        screen = MDScreen(
            name="MainScreen",
        )

        button = MDRectangleFlatButton(
            text="Button 1",
            pos_hint={"center_x": 0.5, "center_y": 0.5},
        )

        button.on_release = self.switch_screen

        screen.add_widget(button)
        return screen


class MainScreenApp(MDApp):
    def build(self):
        Window.size = (350, 600)
        sm = ScreenManager()
        sm.add_widget(SingleButton(sm).build())
        sm.add_widget(SecondScreen().build())

        return sm


if __name__ == "__main__":
    MainScreenApp().run()
