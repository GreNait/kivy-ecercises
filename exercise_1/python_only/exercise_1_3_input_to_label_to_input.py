from multiprocessing import shared_memory
from kivymd.uix.screen import Screen
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDRectangleFlatButton
from kivymd.uix.textfield import MDTextField
from kivymd.app import MDApp
from kivy.uix.screenmanager import ScreenManager
from kivy.core.window import Window

from dataclasses import dataclass, field

@dataclass
class SharedInformation:
    label_text : str
    subscriber : list = field(default_factory=list)

    def add_subscriber(self, subs):
        self.subscriber.append(subs)

    def notify_subscriber(self, val):
        if hasattr(self, "subscriber"):
            for subs in self.subscriber:
                subs.update_label_text(val)

    def __setattr__(self, prop, val):
        super().__setattr__(prop, val)
        if prop == "label_text":
            self.notify_subscriber(val)
        
class AScreen:
    def __init__(self, name: str, shared_information: SharedInformation)->None:
        self.name = name
        self._screen = Screen(name=name)
        self._shared_information = shared_information

    def add_button(self, name, center_x, center_y, callback):
        button = MDRectangleFlatButton(
            text=name,
            pos_hint={"center_x": center_x, "center_y": center_y},
        )

        button.on_release = callback

        self.screen.add_widget(button)

    @property
    def screen(self) -> None:
        return self._screen


class FirstScreen(AScreen):
    def __init__(self, name: str, screen_manager: ScreenManager, shared_information: SharedInformation):
        super().__init__(name=name, shared_information=shared_information)
        super().add_button("Next", 0.5, 0.4, self.next_screen)
        self.add_input(0.5, 0.5)
        self._screen_manager = screen_manager

    def next_screen(self) -> None:
        # self._screen_manager.get_screen(self._screen_manager.next()).children[
        #     0
        # ].text = self.text_input.text
        self._shared_information.label_text = self.text_input.text
        self._screen_manager.current = self._screen_manager.next()

    def add_input(self, center_x, center_y) -> None:
        self.text_input = MDTextField(
            hint_text="Input",
            pos_hint={"center_x": center_x, "center_y": center_y},
            size_hint=(0.5, 1),
        )
        self._screen.add_widget(self.text_input)


class SecondScreen(AScreen):
    def __init__(self, name: str, screen_manager: ScreenManager, shared_information: SharedInformation):
        self._screen_manager = screen_manager
        super().__init__(name=name, shared_information=shared_information)
        super().add_button(
            name="Back", center_x=0.5, center_y=0.4, callback=self.previous_screen
        )
        self.add_label(self._shared_information.label_text)

    def add_label(self, text):
        self.label = MDLabel(
            text=text,
            halign="center",
        )
        self._screen.add_widget(self.label)

    def update_label_text(self, text):
        self.label.text = text

    def previous_screen(self) -> None:
        self._screen_manager.current = self._screen_manager.previous()

class App(MDApp):
    def build(self) -> ScreenManager:
        Window.size = (350, 600)

        sm = ScreenManager()
        shared_information = SharedInformation("Default")
        first_screen=FirstScreen("First Screen", sm, shared_information)
        second_screen=SecondScreen("Second Screen", sm, shared_information)

        shared_information.add_subscriber(second_screen)

        sm.add_widget(first_screen.screen)
        sm.add_widget(second_screen.screen)

        return sm

if __name__ == "__main__":
    App().run()
